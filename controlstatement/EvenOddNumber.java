
package controlstatement;

import java.util.Scanner;

public class EvenOddNumber {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int number;
        System.out.print("Enter a positibe number = ");
        number = input.nextInt();
        
        if (number%2==0) {
            System.out.println("Even");
        }else{
            System.out.println("odd");
        }
    }
}

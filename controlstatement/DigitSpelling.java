package controlstatement;

import java.util.Scanner;

public class DigitSpelling {
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        
        int num;
        System.out.print("Enter any number = ");
        num = inputScanner.nextInt();
        switch(num){
            case 0:
                System.err.println("Zero");
                break;
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
                default:
                    System.out.println("null");
        }
    }
}

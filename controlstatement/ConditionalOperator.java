
package controlstatement;

import java.util.Scanner;

public class ConditionalOperator {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num ,num2,large;
        System.out.print("Enter 2 number = ");
        num = input.nextInt();
        num2 = input.nextInt();
       
        large = (num>num2) ?num:num2;
        System.out.println("Large Number is = "+large);
        
    }
}

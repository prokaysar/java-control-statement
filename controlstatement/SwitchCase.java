package controlstatement;

import java.util.Scanner;

public class SwitchCase {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int marks;
        System.out.print("Enter your marks = ");
        marks = input.nextInt();
        if (marks>=0 && marks<=100) {
            switch(marks/10){
            case 10:
            case 9:
            case 8:
                System.out.println("A+");
                break;
            case 7:
                System.out.println("A-");
                break;
            default:
                System.out.println("Fail");
        }
   
        }else{
            System.out.println("You enter a wrong number!");
        }
    }
}
